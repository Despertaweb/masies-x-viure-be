from django.contrib import admin

from .models import Masia, MasiaImage

# Register your models here in order to be shown in django admin page
admin.site.register(Masia)
admin.site.register(MasiaImage)