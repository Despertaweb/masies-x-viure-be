from rest_framework import serializers
from .models import Masia, MasiaImage

# class ImagesSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = MasiaImage
#         fields = ('images', 'id')
#         print('??????????????????????? ')
#
#
# class MasiaSerializer(serializers.ModelSerializer):
#     images = ImagesSerializer(source='masies', many=True)
#
#     class Meta:
#         model = Masia
#         fields = '__all__'

class MasiaImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = MasiaImage
        fields = ('image',)


class MasiaSerializer(serializers.ModelSerializer):
    images = MasiaImageSerializer(source='masies', many=True, read_only=True)

    class Meta:
        model = Masia
        fields = '__all__'

    def create(self, validated_data):
        images_data = self.context.get('view').request.FILES

        masia = Masia.objects.create(**validated_data)

        print('---------------')
        print('      MASIA           ')
        print('---------------')

        print(images_data)
        for image_data in images_data.values():
            MasiaImage.objects.create(masia=masia, image=image_data)
        return masia
