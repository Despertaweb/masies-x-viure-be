from rest_framework import routers

from .viewsets import MasiaViewSet, MasiaImageViewSet

router = routers.SimpleRouter()

# Define route and it's ViewSet
router.register('masies', MasiaViewSet)
router.register('imatges_masies', MasiaImageViewSet)

urlpatterns = router.urls