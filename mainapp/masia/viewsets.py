from rest_framework import viewsets

from .models import Masia, MasiaImage
from .serializer import MasiaSerializer, MasiaImageSerializer

class MasiaViewSet(viewsets.ModelViewSet):
    queryset = Masia.objects.all()
    serializer_class = MasiaSerializer

class MasiaImageViewSet(viewsets.ModelViewSet):
    queryset = MasiaImage.objects.all()
    serializer_class = MasiaImageSerializer