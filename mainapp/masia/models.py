from django.db import models
from django.template.defaultfilters import slugify


# Create your models here.
class Masia(models.Model):
    title = models.CharField(max_length=50)
    summary = models.CharField(max_length=100)
    description = models.TextField()

    def save(self, *args, **kwargs):
        print('\nself')
        print(self)
        print('\nargs')
        print(*args)
        print('\nkwargs')
        print(*kwargs)
        super(Masia, self).save(*args, **kwargs)

class MasiaImage(models.Model):
    masia = models.ForeignKey(Masia, on_delete=models.CASCADE)
    image = models.FileField(upload_to='masies', blank=True)
